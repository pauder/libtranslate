;<?php/*

[general]
name="LibTranslate"
version="1.12.0.6"
encoding="UTF-8"
description="Translate module from Zend Framework"
description.fr="Librairie partagée permettant le gestion des traductions au format gettext"
long_description.fr="README.md"
delete=0
ov_version="8.1.101"
php_version="5.1.4"
mysql_character_set_database="latin1,utf8"
addon_access_control="0"
author="Paul de Rosanbo (paul.derosanbo@cantico.fr)"
icon="Zend-framework.png"
tags="library,default"

;*/?>